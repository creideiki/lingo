Lingo
=====

Team based word-guessing game.

Short Description:

Lingo is a game based on the TV show of the same name, similar to Wordle but with 2 teams with 2 people trying to guess the words.

Long Description:

Lingo, a TV show that many Dutch people are familiar with, now brought abroad and completely into the furry theme!
The game consists of 2 teams of 2 people who fight against each other by guessing the most degenerate furry terms, in a way much like in Wordle.
The team that guesses the most words will win the game!


Installation
------------

It needs Python3, you can install dependencies with

    pim install -r requirements.txt


Usage
-----

The game has two windows:

The game window is for display only, it shows the game board, timer, score, etc.
You can use this window maximized on a projector.

The second is a controller window where you can manage rounds enter guesses and so on.


### Keyboard Shortcuts

Controller window:

*  _F1_ or _Insert_: Pause timer and focus on the guess input

Game window(s):

* _F5_: Set layout to Board | Clock
* _F6_: Set layout to Clock | Board
* _F7_: Set layout to Clock only
* _F8_: Set layout to Board only
* _F11_: Toggle fullscreen
* _F12_: Flip layout


Configuration
-------------

It needs 3 files, passed as command line arguments:

`--word-file`
A plain text file With 1 word per line which contains words to be used as
solutions for the various rounds.

`--valid-words-file`
Same format as `--word-file`, it's used to determine if a word can be accepted
as a guess.

`--rounds-file`
A JSON file with data about how the rounds are laid out.

They all have default values.


Sessions
--------

Used words are recorded in the file specified by the option
`--used-words-file` (default `./used_words`). Words listed in this
file will not be used again. This supports having a session containing
several invocations of the game (e.g. one every night of a convention)
without reusing words.

To reset the list of used words, remove the used words file. Files
last modified more than 48 hours ago will be automatically deleted.
