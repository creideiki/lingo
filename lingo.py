#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 - 2024 Mattia "Glax"  Basaglia
# SPDX-FileContributor: Mattia "Glax"  Basaglia
#
# SPDX-License-Identifier: GPL-3.0-or-later

import io
import os
import bdb
import sys
import math
import enum
import json
import time
import signal
import random
import pathlib
import argparse
import textwrap
import traceback
from dataclasses import dataclass

from PyQt6 import uic
from PyQt6.QtGui import *
from PyQt6.QtCore import *
from PyQt6.QtWidgets import *
from PyQt6.QtSvgWidgets import *


class Dictionary:
    def __init__(self, used_words_file):
        self.valid_words = set()
        self.words = {}
        self.tagged_words = {}
        self.tags = set()
        self.used_words = set()
        self.used_words_file = used_words_file

    def load_used_words(self):
        if not self.used_words_file.exists():
            return

        session_time = self.used_words_file.stat().st_mtime
        now = time.time()
        if now - session_time > (60 * 60 * 48):
            self.used_words_file.unlink()
            return

        with open(self.used_words_file) as file:
            for word in file:
                self.used_words.add(word.strip().upper())

    def record_used_word(self, word):
        with open(self.used_words_file, 'at+') as file:
            file.write(word + "\n")

    def add_word(self, word, tags):
        self.valid_words.add(word)
        self.tagged_words[word] = tags

    def apply_tags(self, blocked):
        self.words = {}
        for word, tags in self.tagged_words.items():
            if not (blocked & tags) and word not in self.used_words:
                self.add_answer_word(word)

        self.shuffle_answer_words()

    def add_answer_word(self, word):
        if len(word) not in self.words:
            self.words[len(word)] = [word]
        else:
            self.words[len(word)].append(word)

    def word(self, length):
        try:
            word = self.words[length].pop()
        except (IndexError, KeyError):
            # We ran out of words - add the used ones back in
            for w in self.used_words:
                self.add_answer_word(w)
            self.used_words = set()
            os.truncate(self.used_words_file, 0)
            self.shuffle_answer_words()
            word = self.words[length].pop()

        self.record_used_word(word)
        return word

    def load_valid_words(self, word_file):
        with open(word_file) as file:
            for word in file:
                self.valid_words.add(word.strip().upper())

    def load_words(self, word_file, blocked_tags=set()):
        self.words = {}
        self.tagged_words = {}
        self.tags = set()

        with open(word_file) as file:
            for line in file:
                tags = line.strip().split(",")
                word = tags.pop(0).upper()
                tags = set(tags)
                self.tags |= tags
                self.add_word(word, tags)
                if not (blocked_tags & tags) and word not in self.used_words:
                    self.add_answer_word(word)

        self.shuffle_answer_words()

    def shuffle_answer_words(self):
        for word_list in self.words.values():
            random.shuffle(word_list)


class LetterStatus(enum.Enum):
    Empty = enum.auto()
    Guess = enum.auto()
    Wrong = enum.auto()
    WrongPosition = enum.auto()
    Correct = enum.auto()

    def color(self):
        if self == LetterStatus.Correct:
            return QColor("#f01d0a")
        elif self == LetterStatus.WrongPosition:
            return QColor("#fffa48")
        else:
            return QColor("#3250b0")


class LetterItem(QGraphicsObject):
    letter_width = 100
    letter_height = 120
    anim_duration = 500
    anim_letter_delay = 100

    def __init__(self, letter, row, col, status):
        super().__init__()
        self.setPos(col * self.letter_width, row * self.letter_height)
        self.letter = letter
        self.status = status
        self.next_letter = letter
        self.next_status = status
        self._status_transition = 0
        self._anim_group = QSequentialAnimationGroup()
        self._anim_delay = self._anim_group.addPause(0)
        self._anim_group.addPause(self.anim_letter_delay * col)
        self._anim_status = QPropertyAnimation(self, b"status_transition")
        self._anim_group.addAnimation(self._anim_status)
        self._anim_status.setDuration(self.anim_duration)
        self._anim_status.setStartValue(1)
        self._anim_status.setEndValue(0)
        self._anim_status.setEasingCurve(QEasingCurve.Type.InOutQuad)

    @pyqtProperty(float)
    def status_transition(self):
        return self._status_transition

    @status_transition.setter
    def status_transition(self, v):
        self._status_transition = v
        if self._status_transition == 0:
            self.letter = self.next_letter
            self.status = self.next_status
        self.update()

    def boundingRect(self):
        return QRectF(0, 0, self.letter_width, self.letter_height)

    def _paint_background(self, painter):
        painter.setPen(QColor(0, 0, 0, 0))

        painter.setBrush(QColor("#1d2848"))
        painter.drawRect(self.boundingRect().adjusted(-5, -5, 5, 5))
        color = self.status.color()
        if self._status_transition != 0:
            color1 = self.next_status.color()
            color.setRedF(color.redF() * self._status_transition + color1.redF() * (1 - self._status_transition))
            color.setGreenF(color.greenF() * self._status_transition + color1.greenF() * (1 - self._status_transition))
            color.setBlueF(color.blueF() * self._status_transition + color1.blueF() * (1 - self._status_transition))

        painter.setBrush(color)
        painter.drawRoundedRect(self.boundingRect().adjusted(5, 5, -5, -5), 5, 5)

    def _paint_foreground(self, painter, letter, opacity):
        in_box = QRectF(self.letter_width * 0.2, self.letter_height * 0.2, self.letter_width * 0.8, self.letter_height * 0.8)
        font = QFont()
        font.setStyleHint(QFont.StyleHint.Monospace)
        font.setPointSizeF(in_box.width())
        font.setBold(True)

        shape = QPainterPath()
        shape.addText(0, 0, font, letter)
        shape_rect = shape.boundingRect()

        if shape_rect.width() > in_box.width():
            scale = in_box.width() / shape_rect.width()
            transform = QTransform()
            transform.scale(scale, 1)
            shape = transform.map(shape)
            shape_rect = shape.boundingRect()

        shape.translate(self.boundingRect().center() - shape_rect.center())

        painter.setOpacity(opacity)
        painter.setPen(QPen(QColor("#000"), 3))
        painter.setBrush(QColor("#fff"))
        painter.drawPath(shape)

    def paint(self, painter, opt, widget):
        painter.save()

        self._paint_background(painter)

        if self._status_transition > 0.5:
            self._paint_foreground(painter, self.letter, self._status_transition * 2 - 1)
        elif self._status_transition > 0:
            self._paint_foreground(painter, self.next_letter, 1 - self._status_transition * 2)
        elif self.letter:
            self._paint_foreground(painter, self.letter, 1)

        painter.restore()

    def update_letter_status(self, letter, status, delay=0):
        self.next_letter = letter
        self.next_status = status
        self._anim_delay.setDuration(delay)
        self._anim_group.start()
        self.update()


class TimedEffect(QObject):
    _refs = {}
    _id = 0

    def __init__(self, delay, callback, *args, **kwargs):
        super().__init__()
        self.id = TimedEffect._id
        TimedEffect._id += 1
        TimedEffect._refs[self.id] = self
        self.callbacks = [(0, callback, args, kwargs)]
        self.startTimer(delay)

    def add(self, callback, *args, **kwargs):
        self.callbacks.append((0, callback, args, kwargs))
        return self

    def then(self, delay, callback, *args, **kwargs):
        self.callbacks.append((delay, callback, args, kwargs))

    def timerEvent(self, ev):
        self.killTimer(ev.timerId())

        while True:
            _, callback, args, kwargs = self.callbacks.pop(0)
            callback(*args, **kwargs)
            if not self.callbacks or self.callbacks[0][0] > 0:
                break

        if self.callbacks:
            self.startTimer(self.callbacks[0][0])
        else:
            TimedEffect._refs.pop(self.id)
            self.deleteLater()


class GameWindow(QMainWindow):
    class Collection:
        def __init__(self, controller):
            self.controller = controller
            self.windows = []

        def new(self):
            self.windows.append(GameWindow(self.controller))

        def slot(self, name, args=0):
            def callback(*a):
                for window in self.windows:
                    getattr(window, name)(*a[:0])
            return callback

    class Layout(enum.Enum):
        BoardTime = enum.auto()
        TimeBoard = enum.auto()
        Time = enum.auto()
        Board = enum.auto()

    def __init__(self, controller):
        super().__init__(controller)
        uic.loadUi(here / "game.ui", self)
        self.controller = controller
        self.view.setScene(self.controller.scene)
        self.clock_view.setScene(self.controller.clock_scene)
        self.show()
        self.layout = self.Layout.BoardTime

    def maximize(self):
        self.show()
        self.setWindowState(Qt.WindowState.WindowFullScreen)

    def minimize(self):
        self.show()
        self.setWindowState(Qt.WindowState.WindowNoState)

    def set_margin(self, amount):
        self.grid_layout.setContentsMargins(amount, amount, amount, amount)

    def to_layout(self, layout):

        self.layout = layout
        self.grid_layout.removeWidget(self.view)
        self.grid_layout.removeWidget(self.clock_view)

        if layout == self.Layout.Board:
            self.grid_layout.addWidget(self.view, 0, 0)
            self.grid_layout.setColumnStretch(1, 0)
            self.view.show()
            self.clock_view.hide()
        elif layout == self.Layout.Time:
            self.grid_layout.addWidget(self.clock_view, 0, 0)
            self.grid_layout.setColumnStretch(1, 0)
            self.clock_view.show()
            self.view.hide()
        else:
            main = 1
            time = 0
            if layout == self.Layout.BoardTime:
                main, time = time, main
            self.grid_layout.setColumnStretch(time, 1)
            self.grid_layout.setColumnStretch(main, 3)
            self.grid_layout.addWidget(self.clock_view, 0, time)
            self.grid_layout.addWidget(self.view, 0, main)
            self.clock_view.show()
            self.view.show()

    def flip(self):
        if self.layout == self.Layout.BoardTime:
            self.to_layout(self.Layout.TimeBoard)
        elif self.layout == self.Layout.TimeBoard:
            self.to_layout(self.Layout.BoardTime)

    def keyPressEvent(self, e):
        super().keyPressEvent(e)
        if e.key() == Qt.Key.Key_F11:
            if self.windowState() & Qt.WindowState.WindowFullScreen:
                self.setWindowState(Qt.WindowState.WindowNoState)
            else:
                self.setWindowState(Qt.WindowState.WindowFullScreen)
        elif e.key() == Qt.Key.Key_F12:
            self.flip()
        elif e.key() == Qt.Key.Key_F5:
            self.to_layout(self.Layout.BoardTime)
        elif e.key() == Qt.Key.Key_F6:
            self.to_layout(self.Layout.TimeBoard)
        elif e.key() == Qt.Key.Key_F7:
            self.to_layout(self.Layout.Time)
        elif e.key() == Qt.Key.Key_F8:
            self.to_layout(self.Layout.Board)


class ClockItem(QGraphicsObject):
    time_out = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.startTimer(16)
        self.seconds = 0
        self.total_seconds = 30
        self.back = QGraphicsSvgItem(str(here / "clock.svg"))
        self.back.setFlags(self.back.flags() | QGraphicsItem.GraphicsItemFlag.ItemStacksBehindParent)
        self.back.setParentItem(self)
        self.running = False
        self.paused = False

    def reset(self, seconds):
        self.seconds = seconds
        self.total_seconds = seconds
        self.running = True
        self.paused = False
        self.update()

    def to_zero(self):
        self.seconds = 0
        self.running = False
        self.paused = False
        self.update()

    def timerEvent(self, ev):
        if self.running:
            self.seconds -= 16 / 1000
            if self.seconds <= 0:
                self.running = False
                self.seconds = 0
                self.time_out.emit()
            self.update()

    def paint(self, painter, opt, widget):

        painter.setPen(QPen(QColor(0, 0, 0, 0)))
        painter.setBrush(QColor("#3250b0"))
        angle = math.pi / 2 - self.seconds / self.total_seconds * math.pi * 2

        center = QPointF(181, 331)
        hand_radius = 130
        painter.drawEllipse(QPointF(181, 331), 22, 22)
        path = QPainterPath()
        radius = 16
        side_angle = angle + math.pi / 2
        path.moveTo(center + QPointF(math.cos(side_angle) * radius, -math.sin(side_angle) * radius))
        path.lineTo(center + QPointF(math.cos(angle) * hand_radius, -math.sin(angle) * hand_radius))
        side_angle += math.pi
        path.lineTo(center + QPointF(math.cos(side_angle) * radius, -math.sin(side_angle) * radius))
        painter.drawPath(path)

    def boundingRect(self):
        return self.back.boundingRect()

    def pause(self):
        self.running = False
        self.paused = True

    def resume(self):
        if self.seconds > 0:
            self.running = True
        self.paused = False


class GameRound(QObject):
    line_ready = pyqtSignal()
    team_changed = pyqtSignal(int)
    correct_guess = pyqtSignal()
    round_end = pyqtSignal()

    StatePreStart = -1
    StateStart = 0
    StateOtherTeam = 1
    StateFailed = 2

    def __init__(self, team, word, max_guesses, controller):
        super().__init__()
        self.word = word
        self.max_guesses = max_guesses
        self.board = []
        self.autofill_letters = [""] * len(self.word)
        self.current_guess = 0
        self.guesses = []
        self.controller = controller
        self.timer_autofill_start = None
        self.timer_autofill_end = None
        self.state = self.StatePreStart
        self.team = team

        for row in range(max_guesses):
            self._add_board_row(row, 0)

    def start(self):
        if self.word.strip():
            self.reveal(1)
            self.autofill()
        self.state = self.StateStart

    def _add_board_row(self, row, z):
        items = []
        for col in range(len(self.word)):
            item = LetterItem("", row, col, LetterStatus.Empty)
            item.setZValue(z)
            self.controller.scene.addItem(item)
            items.append(item)
        self.board.append(items)

    def reveal(self, count):
        empty_slots = self.autofill_letters.count("")
        count = max(0, min(empty_slots - 1, count))
        for i, letter in enumerate(self.word):
            if count == 0:
                break
            if self.autofill_letters[i] == "":
                self.autofill_letters[i] = letter
                count -= 1

    def autofill(self, emit_ready=True):
        if self.state >= self.StateFailed:
            TimedEffect(self.row_anim_duration(), self.round_end.emit)
        elif emit_ready:
            TimedEffect(self.row_anim_duration(), self.line_ready.emit)

        if self.current_guess >= self.max_guesses:
            self.current_guess = self.max_guesses - 1

        for i, letter in enumerate(self.autofill_letters):
            if letter:
                self.board[self.current_guess][i].update_letter_status(letter, LetterStatus.Correct)
            else:
                self.board[self.current_guess][i].update_letter_status(".", LetterStatus.Empty)

    def finished(self):
        return (
            self.current_guess >= self.max_guesses or
            (self.guesses and self.guesses[-1] == self.word) or
            self.word.strip() == "" or
            self.state >= self.StateFailed
        )

    def enter_guess(self, guess):
        if len(guess) != len(self.word):
            return

        self.guesses.append(guess)

        guess_list = list(guess)
        word_list = list(self.word)

        for i, (gl, wl) in enumerate(zip(guess, self.word)):
            correct = gl == wl
            self.board[self.current_guess][i].update_letter_status(gl, LetterStatus.Correct if correct else LetterStatus.Guess)
            if correct:
                guess_list[i] = ""
                word_list[i] = ""
                self.autofill_letters[i] = wl

        for i, letter in enumerate(guess_list):
            if not letter:
                continue
            if letter in word_list:
                word_list[word_list.index(letter)] = ""
                self.board[self.current_guess][i].update_letter_status(letter, LetterStatus.WrongPosition)
            else:
                self.board[self.current_guess][i].update_letter_status(letter, LetterStatus.Wrong)

        self.current_guess += 1

        if guess == self.word:
            TimedEffect(
                self.row_anim_duration(), self.controller.show_message, "Correct!", GameMessage.message_color, QColor("#fff")
            ).then(GameMessage.time_wait, self.correct_guess.emit)
        elif self.state == self.StateOtherTeam:
            self.state = self.StateFailed
            TimedEffect(
                self.row_anim_duration(), self.controller.show_message, "Too Bad!", GameMessage.message_color, QColor("#fff")
            ).then(GameMessage.time_wait, self.show_result)
        elif self.current_guess == self.max_guesses:
            TimedEffect(self.row_anim_duration(), self.switch_team)
        else:
            TimedEffect(self.row_anim_duration(), self.autofill)

    def switch_team(self):
        self.state = self.StateOtherTeam
        self.team_changed.emit(self.team % 2 + 1)
        self.reveal(1)
        if self.current_guess >= self.max_guesses:
            board_0 = self.board.pop(0)

            for row in self.board:
                for item in row:
                    item._move_anim = QPropertyAnimation(item, b"y")
                    item._move_anim.setDuration(LetterItem.anim_duration)
                    item._move_anim.setEndValue(item.y() - item.letter_height)
                    item._move_anim.setEasingCurve(QEasingCurve.Type.InOutQuad)
                    item._move_anim.start()

            self._add_board_row(self.max_guesses - 1, -1)

        self.autofill()

    def row_anim_duration(self):
        return len(self.word) * LetterItem.anim_letter_delay + LetterItem.anim_duration

    def enter_invalid(self, guess, message, out_of_time):
        guess = guess[:len(self.word)].ljust(len(self.word), ".")
        for i, gl in enumerate(guess):
            self.board[self.current_guess][i].update_letter_status(gl, LetterStatus.Guess)

        TimedEffect(
            self.row_anim_duration() - GameMessage.time_fade_in,
            self.controller.show_message, message, GameMessage.message_color, QColor("#fff"), True
        ).then(GameMessage.time_wait, self.delayed_out_of_time if out_of_time else self.autofill, False)

    def game_area(self):
        return QRectF(
            0, 0,
            LetterItem.letter_width * len(self.word),
            LetterItem.letter_height * self.max_guesses
        )

    def delayed_out_of_time(self, *a):
        self.out_of_time()

    def out_of_time(self):
        if self.state == self.StateStart:
            self.switch_team()
        elif self.state == self.StateOtherTeam:
            self.state = self.StateFailed
            self.controller.show_message("Too Bad!", GameMessage.message_color, QColor("#fff"))
            self.show_result()

    def show_result(self):
        self.autofill_letters = list(self.word)
        self.autofill()


class GameMessage(QGraphicsObject):
    time_fade_in = 600
    time_stay = 800
    time_fade_out = 500
    time_wait = time_fade_in + time_stay
    message_color = QColor("#408")

    def __init__(self, controller):
        super().__init__()
        self.text = ""
        self.fill_color = QColor()
        self.stroke_color = QColor()
        self._fade = 0

        self.font = QFont()
        self.font.setBold(True)
        self.metrics = QFontMetricsF(self.font)
        self.line_height = self.metrics.lineSpacing()

        self._anim_group = QSequentialAnimationGroup()
        anim = QPropertyAnimation(self, b"fade_factor")
        self._anim_group.addAnimation(anim)
        anim.setDuration(self.time_fade_in)
        anim.setStartValue(0)
        anim.setEndValue(1)
        anim.setEasingCurve(QEasingCurve.Type.InOutQuad)

        self._anim_group.addPause(self.time_stay)

        anim = QPropertyAnimation(self, b"fade_factor")
        self._anim_group.addAnimation(anim)
        anim.setDuration(self.time_fade_out)
        anim.setStartValue(1)
        anim.setEndValue(2)
        anim.setEasingCurve(QEasingCurve.Type.InOutQuad)
        anim.finished.connect(self.finished)
        self.on_finished = anim.finished

        self.controller = controller

    def show(self, text, fill_color, stroke_color):
        self.text = text
        self.fill_color = fill_color
        self.stroke_color = stroke_color
        self._anim_group.start()

    def finished(self):
        self.fade_factor = 0
        self.deleteLater()

    @pyqtProperty(float)
    def fade_factor(self):
        return self._fade

    @fade_factor.setter
    def fade_factor(self, v):
        self._fade = v
        if v == 2:
            self._fade = 0
        self.update()

    def boundingRect(self):
        if self._fade == 0:
            return QRectF()
        return self.controller.round.game_area()

    def paint(self, painter, opt, widget):
        if self._fade == 0:
            return

        area = self.controller.round.game_area()
        center = area.center()

        path = QPainterPath()
        for i, line in enumerate(self.text.splitlines()):
            line_path = QPainterPath()
            line_path.addText(QPointF(0, self.line_height * i), self.font, line)
            line_path.translate(-line_path.boundingRect().center().x(), 0)
            path.addPath(line_path)

        trans = QTransform()
        scale = (area.width() - 12) / path.boundingRect().width() * self._fade
        trans.scale(scale, scale)
        path = trans.map(path)

        pos = path.boundingRect().center()
        path.translate(center.x() - pos.x(), center.y() - pos.y())

        if self._fade < 1:
            alpha = self._fade
        else:
            alpha = 2 - self._fade
        painter.setOpacity(alpha)
        painter.setPen(QPen(self.stroke_color, 3))
        painter.setBrush(self.fill_color)
        painter.drawPath(path)


@dataclass
class RoundSettings:
    team: int
    word: int
    guesses: int
    time: int


class RoundStructure(QAbstractTableModel):
    def __init__(self, data):
        super().__init__()
        self.rounds = [RoundSettings(**d) for d in data]
        self.current = -1

    def rowCount(self, parent):
        return len(self.rounds)

    def columnCount(self, parent):
        return 4

    def data(self, index, role=Qt.ItemDataRole.DisplayRole):
        if role != Qt.ItemDataRole.DisplayRole:
            return None

        round = self.rounds[index.row()]
        if index.column() == 0:
            return round.team
        elif index.column() == 1:
            return round.word
        elif index.column() == 2:
            return round.guesses
        elif index.column() == 3:
            return "%s\"" % round.time

        return None

    def headerData(self, section, orientation, role=Qt.ItemDataRole.DisplayRole):
        if orientation == Qt.Orientation.Horizontal and role == Qt.ItemDataRole.DisplayRole:
            if section == 0:
                return "Team"
            elif section == 1:
                return "Length"
            elif section == 2:
                return "Guess"
            elif section == 3:
                return "Time"

        return None

    def next_round(self):
        self.current += 1
        if self.current >= len(self.rounds):
            return None
        return self.rounds[self.current]

    def restart(self):
        self.current = -1
        return self.next_round()

    def current_round(self):
        return self.rounds[self.current]


class ControllerWindow(QMainWindow):
    team_colors = [QColor("#800"), QColor("#080")]
    clock_scene_width = 512
    clock_scene_score_x = clock_scene_width - 20

    def __init__(self, dictionary, round_structure, blocked_tags):
        super().__init__()
        self.team_names = ["Red", "Green"]

        self.dictionary = dictionary
        self.scene = QGraphicsScene()
        self.round_structure = round_structure
        self.blocked_tags = blocked_tags

        self.clock_scene = QGraphicsScene()
        self.clock = ClockItem()
        self.clock_scene.addItem(self.clock)
        self.clock.time_out.connect(self.time_out)

        uic.loadUi(here / "controller.ui", self)
        self.view_round.setModel(self.round_structure)
        self.update_round_settings(self.round_structure.rounds[0])
        self.view_round.selectionModel().selectionChanged.connect(self.deny_selection)
        self.input_guess.returnPressed.connect(self.enter_guess)
        self.score_items = [self.spin_score_1, self.spin_score_2]
        self.score_font = QFont()
        self.score_font.setBold(True)
        self.score_font.setPointSizeF(70)
        self.team_score_items = [
            self.create_score_item(i + 1, color, self.score_items[i])
            for i, color in enumerate(self.team_colors)
        ]
        line_height = self.score_font.pointSizeF() * 2
        y = self.clock.boundingRect().top() - line_height
        self.team_item = self.create_text_item(QColor(), "", 0, y)

        self.game_windows = GameWindow.Collection(self)
        self.game_windows.new()

        self.view.setScene(self.scene)
        self.clock_view.setScene(self.clock_scene)

        self.button_maximize.clicked.connect(self.game_windows.slot("maximize"))
        self.button_minimize.clicked.connect(self.game_windows.slot("minimize"))
        self.button_flip.clicked.connect(self.game_windows.slot("flip"))
        self.button_new_window.clicked.connect(self.game_windows.new)
        self.spin_window_margin.valueChanged.connect(self.game_windows.slot("set_margin", 1))
        self.button_enter_guess.clicked.connect(self.enter_guess)
        self.button_next.clicked.connect(self.new_round)
        self.action_next_round.triggered.connect(self.new_round)
        self.action_start_timer.triggered.connect(self.start_timer)
        self.action_word_stats.triggered.connect(self.show_word_stats)
        self.action_available_words.triggered.connect(self.show_word_list)
        self.action_load_words.triggered.connect(self.load_words)
        self.action_resume_timer.triggered.connect(self.clock.resume)
        self.action_pause_timer.triggered.connect(self.clock.pause)
        self.button_new_game.clicked.connect(self.new_game)
        self.button_end_game.clicked.connect(self.end_board)
        self.edit_team_1.textChanged.connect(self.update_team_names)
        self.edit_team_2.textChanged.connect(self.update_team_names)
        self.button_apply_word_filters.clicked.connect(self.apply_filters)
        self.button_type_guess.clicked.connect(self.clock.pause)

        self.rebuild_tag_filters()

        self.new_game()

    def update_team_names(self):
        self.team_names = [self.edit_team_1.text(), self.edit_team_2.text()]
        if -1 < self.round_structure.current < len(self.round_structure.rounds):
            curr_team = self.round_structure.current_round().team
            self.adjust_team_item(curr_team)
        for i, item in enumerate(self.team_score_items):
            item.setText(self.team_names[i] + ": ")
            self.adjust_font_size(item, self.clock_scene_score_x)

    def adjust_font_size(self, item, max_width):
        item.setFont(self.score_font)
        width = item.sceneBoundingRect().width()
        if width > max_width:
            font = QFont(self.score_font)
            font.setPointSizeF(font.pointSizeF() * max_width / width)
            item.setFont(font)

    def adjust_team_item(self, team):
        self.team_item.setBrush(self.team_colors[team - 1])
        self.team_item.setText(self.team_names[team - 1])
        self.adjust_font_size(self.team_item, self.clock_scene_width)

        icx = self.team_item.boundingRect().center().x()
        ccx = self.clock.boundingRect().center().x()
        self.team_item.setX(ccx - icx)

    def create_text_item(self, color, text, x, y):
        item = QGraphicsSimpleTextItem()
        item.setFont(self.score_font)
        item.setBrush(color)
        item.setPos(x, y)
        item.setText(text)
        self.clock_scene.addItem(item)
        return item

    def create_score_item(self, team, color, spin_box):
        line_height = self.score_font.pointSizeF() * 2
        y = self.clock.boundingRect().bottom() + (team - 1) * line_height + line_height / 2
        item = self.create_text_item(color, "%s: " % self.team_names[team - 1], 0, y)

        x = self.clock_scene_score_x
        score_item = self.create_text_item(color, "0", x, y)
        spin_box.textChanged.connect(score_item.setText)
        return item

    def update_round_settings(self, round_settings):
        self.spin_max_guesses.setValue(round_settings.guesses)
        self.spin_word_length.setValue(round_settings.word)
        self.spin_timer.setValue(round_settings.time)

    def new_game(self):
        self.score_items[0].setValue(0)
        self.score_items[1].setValue(0)

        self.title_board(5, {1: "FURRY", 3: "LINGO"})

    def title_board(self, width, lines, guesses=5):
        self.scene.clear()
        self.round = GameRound(0, width * " ", guesses, self)
        self.team = 0
        self.scene.setSceneRect(self.round.game_area())

        self.team_item.setText("")

        self.start_timer()
        self.clock.running = False

        for row, word in lines.items():
            for col, l in enumerate(word):
                self.round.board[row][col].update_letter_status(l, LetterStatus.Correct)

    def end_board(self):
        s1 = self.spin_score_1.value()
        s2 = self.spin_score_2.value()
        if s1 == s2:
            self.title_board(7, {1: "THE END", 3: " DRAW! "})
            return

        winner = self.team_names[0 if s1 > s2 else 1].upper()
        winner_lines = textwrap.wrap(winner, 7)

        lines = {0: "THE END"}
        for ln, line in enumerate(winner_lines):
            lines[ln + 2] = line.center(7)

        lines[len(winner_lines) + 3] = " WINS! "

        self.title_board(7, lines, len(winner_lines) + 4)

    def new_round(self):
        if self.round.state == self.round.StatePreStart and self.round.word.strip():
            self.round.start()
            return

        if not self.round.finished():
            box = QMessageBox.warning(
                self, "Discard current round?",
                "Do you want to go to the next round?\n(Current round isn't finished yet)",
                QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No
            )

            if box != QMessageBox.StandardButton.Yes:
                return

        self.clock.running = False
        self.input_guess.setText("")

        if not self.check_custom_round.isChecked():
            next_round = self.round_structure.next_round()

            if not next_round:
                self.end_board()
                return

            self.view_round.selectRow(self.round_structure.current)
            self.update_round_settings(next_round)
            self.combo_team.setCurrentIndex(next_round.team - 1)
            self.adjust_team_item(next_round.team)

        self.scene.clear()

        max_guesses = self.spin_max_guesses.value()
        word_length = self.spin_word_length.value()
        team = self.combo_team.currentIndex() + 1
        self.round = GameRound(team, self.dictionary.word(word_length), max_guesses, self)
        self.round.line_ready.connect(self.line_ready)
        self.round.team_changed.connect(self.team_changed)
        self.round.correct_guess.connect(self.correct_guess)
        # self.round.round_end.connect(lambda: self.button_next.setEnabled(True))
        self.scene.setSceneRect(self.round.game_area())
        print(self.round.word)
        self.team_changed(self.round.team, "%s letters" % len(self.round.word), True)

    def enter_guess(self):
        guess = self.input_guess.text().upper()
        self.input_guess.setText("")
        out_of_time = self.clock.seconds == 0 and self.clock.paused

        if self.round.finished():
            QMessageBox.warning(self, "Round Over", "Please Start a new round")
            return

        if len(guess) < len(self.round.word):
            self.round.enter_invalid(guess, "Too Short!", out_of_time)
            return

        if len(guess) > len(self.round.word):
            self.round.enter_invalid(guess, "Too Long!", out_of_time)
            return

        if guess not in self.dictionary.valid_words:
            self.round.enter_invalid(guess, "Invalid Word!", out_of_time)
            return

        # Success
        self.clock.running = False
        self.round.enter_guess(guess)
        return

    def closeEvent(self, e):
        super().closeEvent(e)
        self.game_windows.slot("close")()

    def line_ready(self):
        self.start_timer()

    def start_timer(self):
        self.clock.reset(self.spin_timer.value())

    def time_out(self):
        """
        Called when the timer reaches zero
        """
        self.show_message("Out of Time", GameMessage.message_color, QColor("#fff"))
        TimedEffect(GameMessage.time_wait + 1000, self.out_of_time)

    def out_of_time(self):
        """
        Called after the "out of time" message is shown (plus 1 second buffer)
        """
        if not self.clock.paused:
            self.round.out_of_time()

    def deny_selection(self):
        if self.view_round.currentIndex().row() != self.round_structure.current:
            self.view_round.selectionModel().clearSelection()
            if self.round_structure.current != -1:
                self.view_round.setCurrentIndex(self.round_structure.index(self.round_structure.current, 0))
                self.view_round.selectRow(self.round_structure.current)

    def team_changed(self, team, message="", start_round=False):
        self.team = team
        tail = "\n" + message if message else ""
        msg = self.show_message(self.team_names[team - 1] + tail, self.team_colors[team - 1], QColor("#fff"))
        self.adjust_team_item(team)
        self.clock.to_zero()
        if start_round:
            msg.on_finished.connect(self.round.start)

    def show_message(self, text, fill, stroke, resume_timer=False):
        message = GameMessage(self)
        self.scene.addItem(message)
        message.show(text, fill, stroke)
        if resume_timer:
            self.clock.resume()
        message.on_finished.connect(self.scene.update)
        return message

    def correct_guess(self):
        input = self.score_items[self.team - 1]
        input.setValue(input.value() + 1)

    def show_word_stats(self):
        dialog = QDialog(self)
        uic.loadUi(here / "word_stats.ui", dialog)
        dialog.setParent(self)
        dialog.word_view.setRowCount(len(self.dictionary.words))
        for i, (size, words) in enumerate(sorted(self.dictionary.words.items())):
            dialog.word_view.setItem(i, 0, QTableWidgetItem(str(size)))
            dialog.word_view.setItem(i, 1, QTableWidgetItem(str(len(words))))

        dialog.show()

    def show_word_list(self):
        dialog = WordListDialog(self, self.dictionary)
        dialog.show()

    def load_words(self):
        filename = QFileDialog.getOpenFileName(
            self, "Open word file", str(here)
        )[0]

        if filename:
            self.dictionary.load_words(filename)
            self.rebuild_tag_filters()

    def rebuild_tag_filters(self):
        while True:
            item = self.layout_word_list.takeAt(0)
            if not item:
                break
            item.widget().deleteLater()

        for tag in sorted(self.dictionary.tags):
            checkbox = QCheckBox()
            checkbox.setText(tag)
            checkbox.setChecked(tag not in self.blocked_tags)
            checkbox.toggled.connect(self.toggle_tag)
            self.layout_word_list.addWidget(checkbox)

    def toggle_tag(self, enabled):
        tag = self.sender().text()
        if enabled:
            self.blocked_tags.discard(tag)
        else:
            self.blocked_tags.add(tag)

    def apply_filters(self):
        self.dictionary.apply_tags(self.blocked_tags)

    def keyPressEvent(self, e):
        super().keyPressEvent(e)
        if e.key() in (Qt.Key.Key_F1, Qt.Key.Key_Insert):
            self.button_type_guess.clicked.emit()


class WordListDialog(QDialog):
    def __init__(self, parent, dictionary):
        super().__init__(parent)
        uic.loadUi(here / "word_list.ui", self)
        self.setParent(parent)
        tags = sorted(dictionary.tags)

        self.data = []
        for word_length, word_list in dictionary.words.items():
            for word in word_list:
                word_tags = dictionary.tagged_words[word]
                self.data.append((word, str(word_length)) + tuple(tag if tag in word_tags else "" for tag in tags))

        self.data = sorted(self.data)

        self.raw_model = QStandardItemModel()
        self.raw_model.setColumnCount(2 + len(dictionary.tags))
        self.raw_model.setHorizontalHeaderLabels(["Word", "Length"] + tags)
        self.raw_model.setRowCount(len(self.data))
        for rown, row in enumerate(self.data):
            for coln, value in enumerate(row):
                self.raw_model.setItem(rown, coln, QStandardItem(value))

        self.filter_model = QSortFilterProxyModel()
        self.filter_model.setSourceModel(self.raw_model)
        self.filter_model.setFilterCaseSensitivity(Qt.CaseSensitivity.CaseInsensitive)
        self.filter_model.setFilterKeyColumn(-1)

        self.word_view.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.ResizeToContents)
        self.word_view.setModel(self.filter_model)

        self.input_filter.textChanged.connect(self.filter_model.setFilterFixedString)


def excepthook(etype, evalue, tb):
    if isinstance(evalue, bdb.BdbQuit):
        return
    msg = "\n".join(traceback.format_exception(etype, evalue, tb))
    sys.stderr.write(msg)
    dialog = QDialog()
    layout = QVBoxLayout(dialog)
    info = QPlainTextEdit(dialog)
    layout.addWidget(info)
    info.setPlainText(msg)
    dialog.setWindowTitle("Exception")
    dialog.resize(800, 600)
    dialog.exec()


here = pathlib.Path(__file__).parent


parser = argparse.ArgumentParser(description="Word game")
parser.add_argument(
    "--word-file",
    default=here / "furry_words",
    help="Tagged word file with valid answers"
)
parser.add_argument(
    "--valid-words-file",
    default=here / "all_words",
    help="Additional words considered valid"
)
parser.add_argument(
    "--rounds-file",
    default=here / "rounds.json",
    help="Round structure description",
)
parser.add_argument(
    "--used-words-file",
    default=here / "used_words",
    type=pathlib.Path,
    help="File used to cache words between runs"
)
parser.add_argument(
    "--disabled-tags",
    nargs="+",
    default=["weird", "nsfw"],
    help="Tags from the word file that are disabled by default",
)

if __name__ == "__main__":
    sys.excepthook = excepthook
    ns = parser.parse_args()

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    blocked_tags = set(ns.disabled_tags)
    words = Dictionary(ns.used_words_file)
    words.load_used_words()
    words.load_words(ns.word_file, blocked_tags)
    words.load_valid_words(ns.valid_words_file)
    with open(ns.rounds_file) as rounds_file:
        rounds = RoundStructure(json.load(rounds_file))

    app = QApplication([])

    window = ControllerWindow(words, rounds, blocked_tags)
    window.show()

    sys.exit(app.exec())
