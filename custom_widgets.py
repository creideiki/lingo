# SPDX-FileCopyrightText: 2022 - 2024 Mattia "Glax"  Basaglia
# SPDX-FileContributor: Mattia "Glax"  Basaglia
#
# SPDX-License-Identifier: GPL-3.0-or-later

from PyQt6.QtGui import *
from PyQt6.QtCore import *
from PyQt6.QtWidgets import *


class ResizingGraphicsView(QGraphicsView):
    def fit_view(self):
        if self.scene():
            self.fitInView(self.scene().sceneRect(), Qt.AspectRatioMode.KeepAspectRatio)

    def resizeEvent(self, e):
        super().resizeEvent(e)
        self.fit_view()

    def showEvent(self, e):
        super().showEvent(e)
        self.fit_view()

    def setScene(self, scene):
        super().setScene(scene)
        self.fit_view()
        scene.sceneRectChanged.connect(self.fit_view)
