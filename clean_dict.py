#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 - 2024 Mattia "Glax"  Basaglia
# SPDX-FileContributor: Mattia "Glax"  Basaglia
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import argparse



parser = argparse.ArgumentParser()
parser.add_argument("word_file", nargs="?", default="/usr/share/dict/words")
parser.add_argument("--min-letters", type=int, default=5)
parser.add_argument("--max-letters", type=int, default=7)

ns = parser.parse_args()

valid = re.compile("^[A-Z]+$")

last_word = ""
with open(ns.word_file) as words:
    for word in words:
        word = word.strip().upper()
        if len(word) >= ns.min_letters and len(word) <= ns.max_letters and valid.match(word) and word != last_word:
            print(word)
            last_word = word
