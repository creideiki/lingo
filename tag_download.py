#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 - 2024 Mattia "Glax"  Basaglia
# SPDX-FileContributor: Mattia "Glax"  Basaglia
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import csv
import time
import json
import argparse
import urllib.request


def cat(tag):
    number = tag["category"]
    if number < 0 or number >= len(categories) or number == 2:
        sys.stderr.write("\t%s %s\n" % (tag["name"], number))
        return "cat_%s" % number
    return categories[number]


def row(tag):
    name = tag["name"]
    row = [name, cat(tag)]
    if "_" in name:
        row.append("space")
    if "(" in name:
        row.append("bracket")
    return row


def get_tags(url, page, limit):
    req = urllib.request.Request(url + "?page=%s&limit%s" % (page, limit), headers={
        "User-Agent": "Lingo/1.0 (by glax on e621)"
    })
    resp = urllib.request.urlopen(req)
    data = json.load(resp)
    if isinstance(data, list):
        return [row(tag) for tag in data]
    return []


parser = argparse.ArgumentParser()
parser.add_argument("website")

categories = [
    "general",
    "artist",
    "??",
    "copyright",
    "character",
    "species",
    "invalid",
    "meta",
    "lore",
]
limit = 320
args = parser.parse_args()

url = args.website
if "." not in url:
    url += ".net"
if "://" not in url:
    url = "https://" + url

url += "/tags.json"

page = 1
writer = csv.writer(sys.stdout)
while True:
    sys.stderr.write("%s\n" % page)
    tags = get_tags(url, page, limit)
    if not tags:
        break
    page += 1
    writer.writerows(tags)
    time.sleep(0.9)
